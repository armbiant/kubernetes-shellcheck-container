FROM registry.gitlab.com/akontainers/apkbuild-tools:0.1.20@sha256:3faefd3820181f7978c713ef1bc6badb5260bd325f414277711f86805104d1a5 as apkbuild-tools

FROM alpine:3.19.0@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48 as builder

COPY --from=apkbuild-tools /usr/sbin/builder /usr/sbin/builder
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=shellcheck_0.7 \
    builder prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=shellcheck_0.7 \
    builder build

FROM alpine:3.19.0@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48

# renovate: datasource=repology depName=alpine_3_19/openssl versioning=loose
ARG OPENSSL_VERSION=3.1.4-r3

# hadolint ignore=DL3019
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=shellcheck_0.7 \
    set -eux; \
      apk add \
        libssl3="${OPENSSL_VERSION}" \
        libcrypto3="${OPENSSL_VERSION}" \
      ;

COPY --from=builder /home/abuild/.abuild/*.rsa.pub /etc/apk/keys/

# renovate: datasource=github-tags depName=koalaman/shellcheck
ARG SHELLCHECK_VERSION=0.7.2

# hadolint ignore=DL3018,DL3019
RUN --mount=type=cache,target=/etc/apk/cache,sharing=locked,id=shellcheck_0.7 \
    --mount=type=cache,target=/packages,source=/home/abuild/packages/abuild,from=builder \
    set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add \
        "shellcheck=${SHELLCHECK_VERSION}-r99" \
      ; \
      sed '/^\/packages$/d' -i /etc/apk/repositories;

COPY rootfs /
