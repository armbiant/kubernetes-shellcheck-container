#!/usr/bin/env sh

set -eu

findFilesWithExtension() {
  find "${searchBase}" -type f \( -name '*.bash' -o -name '*.sh' \)
}

findFilesWithShebang() {
  find "${searchBase}" -type f -exec \
    awk 'FNR == 1 && /^#!\s*\/(usr\/)?bin\/(env\s+)?(sh|bash)/ {print FILENAME}' {} \;
}

lintFiles() {
  (
    findFilesWithExtension "${searchBase}"
    findFilesWithShebang "${searchBase}"
  ) | sort -u | (
    status=0
    while IFS= read -r file; do
      printf "Checking %s\n" "$file"
      errors=$(shellcheck --format=gcc "$file") || status=$?
      if [ -z "${errors}" ]; then
        printf "\e[32m%s\e[0m\n" "OK"
      else
        printf "\e[31m%s\e[0m\n" "$errors"
      fi
    done
    return $status
  )
}

main() {
  if [ -n "${1+x}" ] && [ -n "$1" ]; then
    searchBase="$1"
  elif [ -n "${SEARCH_BASE+x}" ] && [ -n "${SEARCH_BASE}" ]; then
    searchBase="${SEARCH_BASE}"
  else
    searchBase='.'
  fi
  lintFiles "${searchBase}"
}

main "$@"
